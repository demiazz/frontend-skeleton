const HtmlPlugin = require("html-webpack-plugin");
const { resolve } = require("path");
const TypeScriptCheckerPlugin = require("fork-ts-checker-webpack-plugin");
const {
  HashedModuleIdsPlugin,
  HotModuleReplacementPlugin
} = require("webpack");

const {
  CACHE_DIRECTORY = "/tmp",
  NODE_ENV = "development",
  WEBPACK_DEV_SERVER_HOST = "localhost",
  WEBPACK_DEV_SERVER_PORT = 8080
} = process.env;

const isProduction = NODE_ENV === "production";
const isDevServer = process.argv.find(arg =>
  arg.includes("webpack-dev-server")
);

const nodeModulesPattern = /[\\/]node_modules[\\/]/;

const config = {
  devtool: isProduction ? "source-map" : "eval",

  entry: {
    portal: resolve(__dirname, "src/index.ts")
  },

  mode: isProduction ? "production" : "development",

  module: {
    rules: [
      {
        exclude: nodeModulesPattern,
        test: /\.(j|t)sx?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: resolve(CACHE_DIRECTORY, "/babel")
            }
          }
        ]
      }
    ]
  },

  output: {
    filename: isProduction ? "[name]-[contenthash].js" : "[name].js",
    publicPath: "/"
  },

  plugins: [
    new HtmlPlugin({
      inject: false,
      template: resolve(__dirname, "src/index.html")
    }),
    new TypeScriptCheckerPlugin({ async: isProduction, formatter: "codeframe" })
  ],

  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"]
  }
};

if (isProduction) {
  config.optimization = {
    runtimeChunk: {
      name: ({ name }) => `${name}-runtime`
    },

    splitChunks: {
      automaticNameDelimiter: "-portal",
      name: true,
      cacheGroups: {
        portalVendor: {
          chunks: ({ name }) => name === "portal",
          enforce: true,
          name: "portal-vendor",
          test: nodeModulesPattern
        }
      }
    }
  };

  config.plugins.push(new HashedModuleIdsPlugin());
}

if (isDevServer) {
  config.devServer = {
    historyApiFallback: true,
    host: WEBPACK_DEV_SERVER_HOST,
    hot: !isProduction,
    hotOnly: false,
    inline: true,
    overlay: true,
    port: parseInt(WEBPACK_DEV_SERVER_PORT, 10)
  };

  if (!isProduction) {
    config.plugins.push(new HotModuleReplacementPlugin());
  }
}

module.exports = config;
